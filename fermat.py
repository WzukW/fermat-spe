#!/bin/python3
# -*- coding: utf-8 -*-

import math

# Exponentiation rapide
def fastExp(n, power, m):
    """Return n^power mod m
    From https://fr.wikipedia.org/wiki/Exponentiation_modulaire and
    http://rosettacode.org/wiki/Modular_exponentiation#Python"""
    return pow(n, power, m)

# PGCD
def pgcd(a, b):
    """PGCD entre a et b, avec inversion de a et b pour que a > b"""
    # Inversion
    if a < b:
        a,b = b,a
    r = b
    while r != 0:
        r = a % b
        # Inversion si on continue
        if r == 0:
            return b
        else:
            a = b
            b = r
    return b

# Test Naif
def naif(number):
    """Test if number is a prime one"""
    # 1 traité à part
    if number == 1:
        return False
    # int(2.1) -> 2; int(2.9) -> 2
    for i in range(1, int(math.sqrt(number)+1)):
        if pgcd (i, number) != 1:
            return False
    return True

# Test Fermat
def fermat(number):
    """Test if number is a prime one with fermat"""
    # 1 traité à part
    if number == 1:
        return False
    # Cache it, used many times
    n_ = number - 1
    for a in range(1, n_):
        # a non premier à n
        if (pgcd(a, number) != 1):
            continue
        if fastExp(a, n_, number) != 1:
            return False
    return True

# Tests
def test_fE():
    """Quelques tests de fastExp"""
    t1 = fastExp(2, 3, 4)
    t2 = fastExp(5, 10, 13)
    t3 = fastExp(100, 1000, 7)
    t4 = fastExp(3, 3, 6)
    if (t1 == 0) & (t2 == 12) & (t3 == 2) & (t4 == 3):
        print("Ok fastExp")
    else:
        print("Error fastExp:", t1, t2, t3, t4)

def test_pgcd():
    """Quelques tests de pgcd"""
    t1 = pgcd(2, 3)
    t2 = pgcd(36, 48)
    t3 = pgcd(6, 8)
    t4 = pgcd(151, 7)
    if (t1 == 1) & (t2 == 12) & (t3 == 2) & (t4 == 1):
        print("Ok PGCD")
    else:
        print("Error PGCD:", t1, t2, t3, t4)

def test_naif():
    """Quelques tests de naif"""
    t1 = naif(2)
    t2 = naif(36)
    t3 = naif(1)
    t4 = naif(135)
    t5 = fermat(4)
    if (t1 == True) & (t2 == False) & (t3 == False) & (t4 == False) & (t5 == False):
        print("Ok Naif")
    else:
        print("Error Naif:", t1, t2, t3, t4, t5)

def test_fermat():
    """Quelques tests de fermat"""
    t1 = fermat(2)
    t2 = fermat(36)
    t3 = fermat(1)
    t4 = fermat(135)
    t5 = fermat(4)
    if (t1 == True) & (t2 == False) & (t3 == False) & (t4 == False) & (t5 == False):
        print("Ok Fermat")
    else:
        print("Error Fermat:", t1, t2, t3, t4, t5)

def debug(msg, m2):
    """Affichage de messages de débogage"""
    dbg = False
    if dbg:
        print(msg, m2)

# Recherche de nombre qui passent l'un mais pas l'autre
def main():
    # Run all
    print("Start")
    print("Tests")
    test_fE()
    test_pgcd()
    test_naif()
    test_fermat()

    # Cherchons un j qui passe fermat mais pas naif
    i = 0
    # Nombre de recherches, 0 pour infinie
    l = 0
    l_ = 1
    while l_ != l :
        l_ = l_ + 1
        f = fermat(i)
        n = naif(i)
        if f != n:
            print("i:", i, "n & f: ", n, f)
        # Affichage de i tt les dix
        if (i % 300) == 0:
            debug("Test de i:", i)
        i += 1

main()
